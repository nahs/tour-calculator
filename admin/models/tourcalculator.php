<?php
//������ �� ������� ��������� � �������
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.model' );

//����� ������ � ��� ����������� ������� �� ����������, ���������� ������
class TourcalculatorModelTourcalculator extends JModel
{
//��������� ���������� $_total � $_pagination � ������; 
//��� ����� ���������� ��������� getTotal() � getPagination(), ��������������. 
 var $_total = null;
 var $_pagination = null;
//��������� � ����������� (��� ������� ���) ��� ���������� - $limitstart � $limit. 
��� ����� ����� ��� ������ JPagination. 
function __construct()
 {
 parent::__construct();
 global $mainframe, $option;
 
 $limit = $mainframe->getUserStateFromRequest('global.list.limit', 'limit', 
$mainframe->getCfg('list_limit'), 'int');
 $limitstart = JRequest::getVar('limitstart', 0, '', 'int');
 
 $limitstart = ($limit != 0 ? (floor($limitstart / $limit) * $limit) : 0);
 
 $this->setState('limit', $limit);
 $this->setState('limitstart', $limitstart);
 }
//����������� ������� getData() � ����������. 
������� �������� ���������� $limitstart � $limit � ����� _getList(). 
��� �������� �������� ������ ������, � �� ��� ������. 
 function getData() 
 {

 if (empty($this->_data)) {
 $query = $this->_buildQuery();
 $this->_data = $this->_getList($query, $this->getState('limitstart'), $this->getState('limit')); 
 }
 return $this->_data;
 }
//�������� ����� ������� getTotal(). 
// � ���� ������� ����� ������������ ����� _getListCount() ������ JModel. 
// ��� ������� ����� ���������� ����� ���������� ����� � �������. 
// ��������, ������� ��� ����� ���������� ����� �������������� � ��������� ������� getPagination().  
 function getTotal()
 {

 if (empty($this->_total)) {
 $query = $this->_buildQuery();
 $this->_total = $this->_getListCount($query);    
 }
 return $this->_total;
 }
//�������� ������� getPagination(). 
//��� ������� ����� ��������� � ���������� ����� ������ Pagination, ������� ����� ������������ � ���. 
 function getPagination()
 {
 if (empty($this->_pagination)) {
 jimport('joomla.html.pagination');
 $this->_pagination = new JPagination($this->getTotal(), $this->getState('limitstart'), 
$this->getState('limit') );
 }
 return $this->_pagination;
 }
//��������� ������ �� ����  
 function _buildQuery()
 {
 $db = $this->getDBO();
$query = "SELECT * FROM ". $db->nameQuote('#__mycomponent')
. $this->_buildQueryOrderBy();
return $query;
 }
//�������� ����� ���� ����� �����������     $orders = array('Name', 'ordering', 'Published', 'id');
 function _buildQueryOrderBy()
{
global $option, $mainframe;
$orders = array('Name', 'ordering', 'Published', 'id');
$filter_order = $mainframe->getUserStateFromRequest($option.'filter_order', 'filter_order', 'published');
 $filter_order_Dir = strtoupper($mainframe->getUserStateFromRequest(
'dbconsultor.filter_order_Dir', 'filter_order_Dir', 'ASC'));
 if ($filter_order_Dir != 'ASC' && $filter_order_Dir != 'DESC')
{
$filter_order_Dir = 'ASC';
}
if (!in_array($filter_order, $orders))
{
$filter_order = 'id';
 }
 return ' ORDER BY '.$filter_order.' '.$filter_order_Dir;
 }
}