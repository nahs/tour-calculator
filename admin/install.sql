CREATE TABLE IF NOT EXISTS `#__tourcalculator_hotel` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'идентификатор отеля',
  `name` mediumtext NOT NULL COMMENT 'Имя отеля',
  `description` mediumtext NOT NULL COMMENT 'Описание отеля',
  `published` tinyint(1) NOT NULL,
  `ordering` VARCHAR(255) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

INSERT INTO #__tourcalculator_hotel
(`name`, `description`, `published`, `ordering`)
VALUES
(1,'Бештау', 'Гостиница класса люкс со всеми удобствами', '1', '1');

CREATE TABLE IF NOT EXISTS `#__tourcalculator_rooms` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'идентификатор номера',
  `id_hotel` int(11) NOT NULL COMMENT 'идентификатор отеля',
  `name` text NOT NULL COMMENT 'Имя номера(номер)',
  `cena_nomera` int(11) NOT NULL COMMENT 'Основная цена',
  `cena_nomera_end` int(11) NOT NULL COMMENT 'цена в выходные',
  `cena_nomera_zima` int(11) NOT NULL COMMENT 'цена в низкий сезон',
  `cena_nomera_odnomest` int(11) NOT NULL COMMENT 'цена при одноместном размещении',
  `zavtrak_hotel` enum('0','1') NOT NULL COMMENT 'наличие завтрака да\\нет',
  `nomer_pozdno` int(11) NOT NULL COMMENT 'доплата за поздний выезд',
  `nomer_krovat` int(11) NOT NULL COMMENT 'стоимость дополнительной кровати',
  `description` mediumtext NOT NULL COMMENT 'описание',
  `published` tinyint(1) NOT NULL,
  `ordering` VARCHAR(255) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

INSERT INTO `#__tourcalculator_rooms` (`id`, `id_hotel`, `name`, `cena_nomera`, `cena_nomera_end`, `cena_nomera_zima`, `cena_nomera_odnomest`, `zavtrak_hotel`, `nomer_pozdno`, `nomer_krovat`, `description`, `published`, `ordering`) VALUES
(1, 1, 'Двухуровневый номер люкс', 10300, 10300, 10300, 9500, '1', 0, 0, '', '1', '1'),
(2, 1, 'Одноместный номер', 2100, 2100, 1800, 1800, '0', 0, 0, '', '1', '1'),
(3, 1, 'Двухкомнатный номер люкс', 7600, 7600, 7600, 6800, '1', 0, 0, '', '1', '1'),
(4, 1, 'Стандартный Бизнес', 5000, 5000, 5000, 3800, '1', 0, 0, '', '1', '1'),
(5, 1, 'Стандартный Studio', 5700, 5700, 5700, 4500, '0', 0, 0, '', '1', '1'),
(6, 1, 'Стандартный номер «Комфорт»', 4500, 4500, 4500, 3300, '1', 0, 0, '', '1', '1'),
(7, 1, 'Стандартный номер', 4200, 4200, 4200, 3000, '0', 0, 0, '', '1', '1'),
(8, 1, 'Одноместный стандарт', 2800, 2800, 2800, 2800, '0', 0, 0, '', '1', '1');